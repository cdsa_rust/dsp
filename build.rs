/// Purpose: Detects if the target is a Cortex-M7 or Cortex-M33 and enables
/// the vsqrt feature if so.
use std::env;

fn main() {
    let target = env::var("TARGET").unwrap();

    if target == "thumbv7em-none-eabihf" || target == "thumbv8m.main-none-eabihf" {
        // When using optimization, compiler print: error: instruction requires: VFP2
        // However, the code is still generated and works fine.
        println!("cargo:rustc-cfg=vabs");
        println!("cargo:rustc-cfg=vsqrt");
    }
}
