use approx::assert_abs_diff_eq;
use dsp::iir::IIRFilter;

const N_TAPS: usize = 9;
const B_TAPS: [f32; N_TAPS] = [
    1.238_729_7e-5,
    9.909_837_5e-5,
    0.000_346_844_32,
    0.000_693_688_63,
    0.000_867_110_8,
    0.000_693_688_63,
    0.000_346_844_32,
    9.909_837_5e-5,
    1.238_729_7e-5,
];
const A_TAPS: [f32; N_TAPS] = [
    1.0,
    -5.926_996_7,
    16.694_735,
    -28.885_399,
    33.408_936,
    -26.394_197,
    13.912_07,
    -4.484_505,
    0.680_996_3,
];

const INPUT_SIZE: usize = 35;
const RANDOM_DATA: [f32; INPUT_SIZE] = [
    0.905_386_15,
    0.851_791_6,
    0.851_650_66,
    0.720_381_2,
    0.879_837_9,
    0.352_221_22,
    0.518_321_9,
    0.034_391_876,
    0.602_028_6,
    0.826_400_9,
    0.941_353_7,
    0.607_761_6,
    0.110_817_61,
    0.913_816_15,
    0.111_439_01,
    0.717_208_5,
    0.706_087_6,
    0.242_634_48,
    0.003_256_742_6,
    0.570_696_7,
    0.104_208_95,
    0.407_418_55,
    0.298_360_85,
    0.152_282_37,
    0.788_724,
    0.229_221_61,
    0.111_774_31,
    0.621_978_8,
    0.231_977_42,
    0.561_859_7,
    0.027_910_529,
    0.102_749_825,
    0.997_616_1,
    0.263_878_94,
    0.919_122_16,
];
const EXPECTED_FILTERED_RANDOM_DATA: [f32; INPUT_SIZE] = [
    1.121_528_75e-5,
    0.000_166_746_66,
    0.001_210_059_5,
    0.005_729_001,
    0.019_949_626,
    0.054_574_3,
    0.122_200_34,
    0.230_084_57,
    0.370_719_37,
    0.516_454_94,
    0.624_644_9,
    0.654_800_3,
    0.591_108_44,
    0.457_506_54,
    0.313_384_47,
    0.227_490_26,
    0.241_515_56,
    0.344_308_47,
    0.474_639_4,
    0.554_310_74,
    0.533_339_3,
    0.418_948_17,
    0.268_931_93,
    0.153_262_2,
    0.109_706_71,
    0.123_624_645,
    0.145_073_97,
    0.129_796_37,
    0.073_696_844,
    0.015_543_388,
    0.006_235_414,
    0.067_849_97,
    0.173_295_38,
    0.262_072_77,
    0.281_008_42,
];

#[test]
fn filtering_random_data() {
    let mut iir = IIRFilter::new(&B_TAPS, &A_TAPS);
    let output: Vec<_> = RANDOM_DATA.iter().map(|x| iir.filter(x)).collect();

    assert_eq!(output.len(), INPUT_SIZE);
    const TOLERANCE: f32 = 0.0005;
    for i in 0..output.len() {
        assert_abs_diff_eq!(
            output[i],
            EXPECTED_FILTERED_RANDOM_DATA[i],
            epsilon = TOLERANCE
        );
    }
}

#[test]
fn filtering_random_data_scale_output() {
    let scaling_factor: f32 = 2.0;
    let mut a_tap = A_TAPS;
    let mut b_tap = B_TAPS;
    a_tap.iter_mut().for_each(|x| *x *= scaling_factor);
    b_tap.iter_mut().for_each(|x| *x *= scaling_factor);
    let mut iir = IIRFilter::new(&b_tap, &a_tap);
    let output: Vec<_> = RANDOM_DATA.iter().map(|x| iir.filter(x)).collect();

    assert_eq!(output.len(), INPUT_SIZE);

    const TOLERANCE: f32 = 0.002;
    for i in 0..output.len() {
        assert_abs_diff_eq!(
            output[i],
            EXPECTED_FILTERED_RANDOM_DATA[i],
            epsilon = TOLERANCE
        );
    }
}
