import random

import numpy as np
from scipy.signal import unit_impulse, lfilter, firwin


# Filter design
num_taps = 32
cutoff = 0.1
taps = firwin(num_taps, cutoff)

# Filter dirac with the FIR filter.
dirac_x = unit_impulse(num_taps)
filtered_dirac = lfilter(taps, 1.0, dirac_x)

# Save the filter coefficients and the filtered dirac.
with open("fir_dirac.txt", 'w+') as f:
    f.writelines("taps: ")
    f.write("[")
    f.write(", ".join([str(x) for x in taps]))
    f.write("]\n")

    f.writelines("filtered_dirac: ")
    f.write("[")
    f.write(", ".join([str(x) for x in filtered_dirac]))
    f.write("]\n")

# Random data
random_x = [random.random() for _ in range(0, 32 * 2 + 3)]
filtered_random = lfilter(taps, 1.0, random_x)

# Save the filter coefficients and the filtered random data.
with open("fir_random.txt", 'w+') as f:
    f.writelines("taps: ")
    f.write("[")
    f.write(", ".join([str(x) for x in taps]))
    f.write("]\n")

    f.writelines("random_x: ")
    f.write("[")
    f.write(", ".join([str(x) for x in random_x]))
    f.write("]\n")

    f.writelines("filtered_random: ")
    f.write("[")
    f.write(", ".join([str(x) for x in filtered_random]))
    f.write("]\n")

# Sine data
t = np.linspace(0, 1, 32 * 2 + 3, endpoint=False)
freq = np.pi / 10
sine_x = np.sin(2*np.pi*freq*t)
filtered_sine = lfilter(taps, 1.0, sine_x)
filtered_sine_decimated = [x for i, x in enumerate(filtered_sine) if i % 4 == 0]


# Interpolation with a 4x ratio.
sine_x_padded = [[x, 0.0, 0.0, 0.0] for x in sine_x]
sine_x_padded = list(np.concatenate(sine_x_padded).flat)
filtered_sine_x_interpolation = lfilter(taps, 1.0, sine_x_padded)
filtered_sine_x_interpolation_scaled = [x * 4 for x in filtered_sine_x_interpolation]

# Save the filter coefficients and the filtered sine data.
with open("fir_sine.txt", 'w+') as f:
    f.writelines("taps: ")
    f.write("[")
    f.write(", ".join([str(x) for x in taps]))
    f.write("]\n")

    f.writelines("sine_x: ")
    f.write("[")
    f.write(", ".join([str(x) for x in sine_x]))
    f.write("]\n")

    f.writelines("filtered_sine_decimation: ")
    f.write("[")
    f.write(", ".join([str(x) for x in filtered_sine_decimated]))
    f.write("]\n")

    f.writelines("sine_x_padded: ")
    f.write("[")
    f.write(", ".join([str(x) for x in sine_x_padded]))
    f.write("]\n")

    f.writelines("filtered_sine_interpolation: ")
    f.write("[")
    f.write(", ".join([str(x) for x in filtered_sine_x_interpolation_scaled]))
    f.write("]\n")
