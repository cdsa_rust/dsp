import random
from scipy.signal import lfilter, iirfilter

# Filter design
num_taps = 8
cutoff = 0.3
b, a = iirfilter(num_taps, cutoff, rp=5, btype='lowpass', ftype='cheby1')

# Random data
random_x = [random.random() for _ in range(0, 32 + 3)]
filtered_random = lfilter(b, a, random_x)

# Save the filter coefficients and the filtered random data.
with open("iir_random.txt", 'w+') as f:
    f.writelines("b: ")
    f.write("[")
    f.write(", ".join([str(x) for x in b]))
    f.write("]\n")

    f.writelines("a: ")
    f.write("[")
    f.write(", ".join([str(x) for x in a]))
    f.write("]\n")

    f.writelines("random_x: ")
    f.write("[")
    f.write(", ".join([str(x) for x in random_x]))
    f.write("]\n")

    f.writelines("filtered_random: ")
    f.write("[")
    f.write(", ".join([str(x) for x in filtered_random]))
    f.write("]\n")
