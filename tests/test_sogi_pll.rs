// CSV Format:
// time[sec], Input, theta, |V'|

// Log index: 3: Input, 2: Output (theta), 1: |V'| (d)
// To generate the test data, launch the sogi_pll.slx model in Matlab and run the following command
// in the Matlab command window:
// dlmwrite('sogi_pll.csv', [logsout{3}.Values.Time, logsout{3}.Values.Data, logsout{2}.Values.Data, logsout{1}.Values.Data], 'precision', 18)

#![cfg(test)]

use approx::assert_abs_diff_eq;
use dsp::sogi_pll::SogiPll;

const SOGI_PLL_CSV: &str = "tests/data/sogi_pll.csv";
const INPUT_SIGNAL_INDEX: usize = 1;
const OUTPUT_THETA_INDEX: usize = 2;
const OUTPUT_D_INDEX: usize = 3;
const TOLERANCE_SINE: f32 = 0.0002;
const TOLERANCE_V: f32 = 0.03;

#[test]
fn test_sogi_pll() {
    // Read CSV file
    let mut rdr = csv::Reader::from_path(SOGI_PLL_CSV).unwrap();
    let mut input_signal = Vec::new();
    let mut expected_output_theta = Vec::new();
    let mut expected_output_d = Vec::new();

    for result in rdr.records() {
        let record = result.unwrap();
        input_signal.push(record[INPUT_SIGNAL_INDEX].parse::<f32>().unwrap());
        expected_output_theta.push(record[OUTPUT_THETA_INDEX].parse::<f32>().unwrap());
        expected_output_d.push(record[OUTPUT_D_INDEX].parse::<f32>().unwrap());
    }

    // Run the PLL
    let mut output_theta = Vec::new();
    let mut output_d = Vec::new();

    let center_freq = 60.0 * 2.0 * std::f32::consts::PI;
    let k_sogi = 0.8;
    let kp = 100.0;
    let ki = 500.0;
    let dt = 100e-6;
    let mut pll = SogiPll::new(center_freq, k_sogi, kp, ki, dt);

    for i in input_signal.iter() {
        let (theta, d) = pll.update(*i);
        output_theta.push(theta);
        output_d.push(d);
    }

    // Save output to CSV file
    let mut wtr = csv::Writer::from_path("tests/data/sogi_pll_output.csv").unwrap();
    for i in 0..output_theta.len() {
        wtr.write_record(&[
            output_theta[i].to_string(),
            expected_output_theta[i].to_string(),
        ])
        .unwrap();
    }

    // Assert outputs
    for i in 0..expected_output_theta.len() {
        assert_abs_diff_eq!(
            output_theta[i],
            expected_output_theta[i],
            epsilon = TOLERANCE_SINE
        );
        assert_abs_diff_eq!(output_d[i], expected_output_d[i], epsilon = TOLERANCE_V);
    }
}
