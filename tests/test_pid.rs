// Use a MATLAB model to generate test data
// Model: set point -> +/- -> PID -> [0.5 / (z - 0.02)] ---> output
//                      ^________________________________|
// CSV format: time[sec], Set point, Output

// To save data from MATLAB:
// dlmwrite('step_no_anti_windup.csv', [data_no_anti_windup.Time, data_no_anti_windup.Data], 'precision', 18)
// dlmwrite('step_anti_windup.csv', [data_anti_windup.Time, data_anti_windup.Data], 'precision', 18)

// PID parameters:
// kp: 1.8
// ki: 1000
// kd: 0.00001
// Sample time: 0.001 seconds
// Saturation: +/- 2.0

use approx::assert_abs_diff_eq;
use dsp::pid_controller::{PIDController, PIDControllerConfig};
use num::Float;

const F32_TOLERANCE: f32 = 1e-6;
const F64_TOLERANCE: f64 = 1e-6;

static SET_POINT_IDX: usize = 1;
static OUTPUT_IDX: usize = 2;

struct Plant<T>
where
    T: Float,
{
    b0: T,
    a0: T,
    output: T,
    last_input: T,
}

impl<T> Plant<T>
where
    T: Float,
{
    fn new() -> Plant<T> {
        Plant {
            b0: T::from(0.5).unwrap(),
            a0: T::from(0.02).unwrap(),
            output: T::zero(),
            last_input: T::zero(),
        }
    }

    fn update(&mut self) -> T {
        self.output = self.last_input * self.b0 + self.a0 * self.output;
        self.output
    }

    fn capture_input(&mut self, input: T) {
        self.last_input = input;
    }
}

struct System<T>
where
    T: Float,
{
    pid: PIDController<T>,
    plant: Plant<T>,
}

impl<T> System<T>
where
    T: Float,
{
    fn new(anti_windup: bool) -> System<T> {
        let p_saturation;
        let n_saturation;

        if anti_windup {
            p_saturation = T::from(2.0).unwrap();
            n_saturation = T::from(-2.0).unwrap();
        } else {
            p_saturation = T::infinity();
            n_saturation = T::neg_infinity();
        }
        let config = PIDControllerConfig {
            kp: T::from(1.8).unwrap(),
            ki: T::from(1000.0).unwrap(),
            kd: T::from(0.00001).unwrap(),
            sample_time: T::from(0.001).unwrap(),
            positive_saturation: p_saturation,
            negative_saturation: n_saturation,
        };
        System {
            pid: PIDController::new(&config),
            plant: Plant::new(),
        }
    }

    fn update(&mut self, set_point: T) -> T {
        // Update output
        let output = self.plant.update();
        let error = set_point - output;
        let pid_output = self.pid.update(error);
        self.plant.capture_input(pid_output);
        output
    }
}

#[test]
fn run_pid_reach_target_f32() {
    // Read CSV file
    let mut rdr = csv::Reader::from_path("tests/data/step_no_anti_windup.csv").unwrap();
    let mut set_points = Vec::new();
    let mut expected_output = Vec::new();

    for result in rdr.records() {
        let record = result.unwrap();
        let set_point: f32 = record[SET_POINT_IDX].parse().unwrap();
        let output: f32 = record[OUTPUT_IDX].parse().unwrap();
        set_points.push(set_point);
        expected_output.push(output);
    }

    // Create system
    let mut system: System<f32> = System::new(false);
    let mut output = Vec::new();

    // Run system
    for set_point in set_points {
        output.push(system.update(set_point));
    }

    // Assert output
    for i in 0..expected_output.len() {
        assert_abs_diff_eq!(output[i], expected_output[i], epsilon = F32_TOLERANCE);
    }
}

#[test]
fn run_pid_reach_target_f64() {
    // Read CSV file
    let mut rdr = csv::Reader::from_path("tests/data/step_no_anti_windup.csv").unwrap();
    let mut set_points = Vec::new();
    let mut expected_output = Vec::new();
    for result in rdr.records() {
        let record = result.unwrap();
        let set_point: f64 = record[SET_POINT_IDX].parse().unwrap();
        let output: f64 = record[OUTPUT_IDX].parse().unwrap();
        set_points.push(set_point);
        expected_output.push(output);
    }

    // Create system
    let mut system: System<f64> = System::new(false);
    let mut output = Vec::new();

    // Run system
    for set_point in set_points {
        output.push(system.update(set_point));
    }

    // Assert output
    for i in 0..expected_output.len() {
        assert_abs_diff_eq!(output[i], expected_output[i], epsilon = F64_TOLERANCE);
    }
}

#[test]
fn hit_anti_windup_f32() {
    // Read CSV file
    let mut rdr = csv::Reader::from_path("tests/data/step_anti_windup.csv").unwrap();
    let mut set_points = Vec::new();
    let mut expected_output = Vec::new();
    for result in rdr.records() {
        let record = result.unwrap();
        let set_point: f32 = record[SET_POINT_IDX].parse().unwrap();
        let output: f32 = record[OUTPUT_IDX].parse().unwrap();
        set_points.push(set_point);
        expected_output.push(output);
    }

    // Create system
    let mut system: System<f32> = System::new(true);
    let mut output = Vec::new();

    // Run system
    for set_point in set_points {
        output.push(system.update(set_point));
    }

    // Assert output
    for i in 0..expected_output.len() {
        assert_abs_diff_eq!(output[i], expected_output[i], epsilon = F32_TOLERANCE);
    }
}

#[test]
fn hit_anti_windup_f64() {
    // Read CSV file
    let mut rdr = csv::Reader::from_path("tests/data/step_anti_windup.csv").unwrap();
    let mut set_points = Vec::new();
    let mut expected_output = Vec::new();
    for result in rdr.records() {
        let record = result.unwrap();
        let set_point: f64 = record[SET_POINT_IDX].parse().unwrap();
        let output: f64 = record[OUTPUT_IDX].parse().unwrap();
        set_points.push(set_point);
        expected_output.push(output);
    }

    // Create system
    let mut system: System<f64> = System::new(true);
    let mut output = Vec::new();

    // Run system
    for set_point in set_points {
        output.push(system.update(set_point));
    }

    // Assert output
    for i in 0..expected_output.len() {
        assert_abs_diff_eq!(output[i], expected_output[i], epsilon = F64_TOLERANCE);
    }
}
