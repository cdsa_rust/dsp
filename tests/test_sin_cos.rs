use approx::assert_abs_diff_eq;
use dsp::math::table;
use dsp::math::{cos, cos_lut, sin, sin_cos, sin_lut};
use num::range;
use rand::Rng;

const TOLERANCE: f32 = 1e-6;
const MIN_ANGLE_RANGE: f32 = -720.0;
const MAX_ANGLE_RANGE: f32 = 720.0;

fn to_rad(deg: f32) -> f32 {
    deg * core::f32::consts::PI / 180.0
}

#[test]
fn test_sin() {
    let angle: Vec<f32> = range(MIN_ANGLE_RANGE, MAX_ANGLE_RANGE)
        .map(to_rad)
        .collect();

    for angle in angle.iter() {
        assert_abs_diff_eq!(sin(*angle), angle.sin(), epsilon = TOLERANCE);
    }
}

#[test]
fn test_sin_rand_value() {
    let mut rng = rand::thread_rng();
    for _ in 0..10000 {
        let angle: f32 =
            to_rad(rng.gen_range(-2.0 * std::f32::consts::PI..2.0 * std::f32::consts::PI));
        assert_abs_diff_eq!(sin(angle), angle.sin(), epsilon = TOLERANCE);
    }
}

#[test]
fn test_sin_invalid_angle() {
    assert!(sin(f32::INFINITY).is_nan());
    assert!(sin(f32::NEG_INFINITY).is_nan());
    assert!(sin(f32::NAN).is_nan());
}

#[test]
fn test_cos() {
    let angle: Vec<f32> = range(MIN_ANGLE_RANGE, MAX_ANGLE_RANGE)
        .map(to_rad)
        .collect();

    for angle in angle.iter() {
        assert_abs_diff_eq!(cos(*angle), angle.cos(), epsilon = TOLERANCE);
    }
}

#[test]
fn test_cos_rand_value() {
    let mut rng = rand::thread_rng();
    for _ in 0..10000 {
        let angle: f32 =
            to_rad(rng.gen_range(-2.0 * std::f32::consts::PI..2.0 * std::f32::consts::PI));
        assert_abs_diff_eq!(cos(angle), angle.cos(), epsilon = TOLERANCE);
    }
}

#[test]
fn test_cos_invalid_angle() {
    assert!(cos(f32::INFINITY).is_nan());
    assert!(cos(f32::NEG_INFINITY).is_nan());
    assert!(cos(f32::NAN).is_nan());
}

#[test]
fn test_sin_cos() {
    let angle: Vec<f32> = range(MIN_ANGLE_RANGE, MAX_ANGLE_RANGE)
        .map(to_rad)
        .collect();

    for angle in angle.iter() {
        let (s, c) = sin_cos(*angle);
        assert_abs_diff_eq!(s, angle.sin(), epsilon = TOLERANCE);
        assert_abs_diff_eq!(c, angle.cos(), epsilon = TOLERANCE);
    }
}

#[test]
fn test_sin_cos_rand_value() {
    let mut rng = rand::thread_rng();
    for _ in 0..10000 {
        let angle: f32 =
            to_rad(rng.gen_range(-2.0 * std::f32::consts::PI..2.0 * std::f32::consts::PI));
        let (s, c) = sin_cos(angle);
        assert_abs_diff_eq!(s, angle.sin(), epsilon = TOLERANCE);
        assert_abs_diff_eq!(c, angle.cos(), epsilon = TOLERANCE);
    }
}

#[test]
fn test_sin_cos_invalid_angle() {
    let (s, c) = sin_cos(f32::INFINITY);
    assert!(s.is_nan());
    assert!(c.is_nan());

    let (s, c) = sin_cos(f32::NEG_INFINITY);
    assert!(s.is_nan());
    assert!(c.is_nan());

    let (s, c) = sin_cos(f32::NAN);
    assert!(s.is_nan());
    assert!(c.is_nan());
}

#[test]
fn test_sin_lut_32() {
    const TOLERANCE: f32 = 1e-2;
    const TABLE: &[f32] = table::SIN_COS_TABLE_32;
    let angle: Vec<f32> = range(MIN_ANGLE_RANGE, MAX_ANGLE_RANGE)
        .map(to_rad)
        .collect();

    for angle in angle.iter() {
        assert_abs_diff_eq!(sin_lut(*angle, TABLE), angle.sin(), epsilon = TOLERANCE);
    }
}

#[test]
fn test_sin_lut_64() {
    const TOLERANCE: f32 = 1e-2;
    const TABLE: &[f32] = table::SIN_COS_TABLE_64;
    let angle: Vec<f32> = range(MIN_ANGLE_RANGE, MAX_ANGLE_RANGE)
        .map(to_rad)
        .collect();

    for angle in angle.iter() {
        assert_abs_diff_eq!(sin_lut(*angle, TABLE), angle.sin(), epsilon = TOLERANCE);
    }
}

#[test]
fn test_sin_lut_128() {
    const TOLERANCE: f32 = 1e-3;
    const TABLE: &[f32] = table::SIN_COS_TABLE_128;
    let angle: Vec<f32> = range(MIN_ANGLE_RANGE, MAX_ANGLE_RANGE)
        .map(to_rad)
        .collect();

    for angle in angle.iter() {
        assert_abs_diff_eq!(sin_lut(*angle, TABLE), angle.sin(), epsilon = TOLERANCE);
    }
}

#[test]
fn test_sin_lut_256() {
    const TOLERANCE: f32 = 1e-4;
    const TABLE: &[f32] = table::SIN_COS_TABLE_256;
    let angle: Vec<f32> = range(MIN_ANGLE_RANGE, MAX_ANGLE_RANGE)
        .map(to_rad)
        .collect();

    for angle in angle.iter() {
        assert_abs_diff_eq!(sin_lut(*angle, TABLE), angle.sin(), epsilon = TOLERANCE);
    }
}

#[test]
fn test_sin_lut_512() {
    const TOLERANCE: f32 = 1e-4;
    const TABLE: &[f32] = table::SIN_COS_TABLE_512;
    let angle: Vec<f32> = range(MIN_ANGLE_RANGE, MAX_ANGLE_RANGE)
        .map(to_rad)
        .collect();

    for angle in angle.iter() {
        assert_abs_diff_eq!(sin_lut(*angle, TABLE), angle.sin(), epsilon = TOLERANCE);
    }
}

#[test]
fn test_sin_lut_1024() {
    const TOLERANCE: f32 = 1e-5;
    const TABLE: &[f32] = table::SIN_COS_TABLE_1024;
    let angle: Vec<f32> = range(MIN_ANGLE_RANGE, MAX_ANGLE_RANGE)
        .map(to_rad)
        .collect();

    for angle in angle.iter() {
        assert_abs_diff_eq!(sin_lut(*angle, TABLE), angle.sin(), epsilon = TOLERANCE);
    }
}

#[test]
fn test_cos_lut_32() {
    const TOLERANCE: f32 = 1e-2;
    const TABLE: &[f32] = table::SIN_COS_TABLE_32;
    let angle: Vec<f32> = range(MIN_ANGLE_RANGE, MAX_ANGLE_RANGE)
        .map(to_rad)
        .collect();

    for angle in angle.iter() {
        assert_abs_diff_eq!(cos_lut(*angle, TABLE), angle.cos(), epsilon = TOLERANCE);
    }
}

#[test]
fn test_cos_lut_64() {
    const TOLERANCE: f32 = 1e-2;
    const TABLE: &[f32] = table::SIN_COS_TABLE_64;
    let angle: Vec<f32> = range(MIN_ANGLE_RANGE, MAX_ANGLE_RANGE)
        .map(to_rad)
        .collect();

    for angle in angle.iter() {
        assert_abs_diff_eq!(cos_lut(*angle, TABLE), angle.cos(), epsilon = TOLERANCE);
    }
}

#[test]
fn test_cos_lut_128() {
    const TOLERANCE: f32 = 1e-3;
    const TABLE: &[f32] = table::SIN_COS_TABLE_128;
    let angle: Vec<f32> = range(MIN_ANGLE_RANGE, MAX_ANGLE_RANGE)
        .map(to_rad)
        .collect();

    for angle in angle.iter() {
        assert_abs_diff_eq!(cos_lut(*angle, TABLE), angle.cos(), epsilon = TOLERANCE);
    }
}

#[test]
fn test_cos_lut_256() {
    const TOLERANCE: f32 = 1e-4;
    const TABLE: &[f32] = table::SIN_COS_TABLE_256;
    let angle: Vec<f32> = range(MIN_ANGLE_RANGE, MAX_ANGLE_RANGE)
        .map(to_rad)
        .collect();

    for angle in angle.iter() {
        assert_abs_diff_eq!(cos_lut(*angle, TABLE), angle.cos(), epsilon = TOLERANCE);
    }
}

#[test]
fn test_cos_lut_512() {
    const TOLERANCE: f32 = 1e-4;
    const TABLE: &[f32] = table::SIN_COS_TABLE_512;
    let angle: Vec<f32> = range(MIN_ANGLE_RANGE, MAX_ANGLE_RANGE)
        .map(to_rad)
        .collect();

    for angle in angle.iter() {
        assert_abs_diff_eq!(cos_lut(*angle, TABLE), angle.cos(), epsilon = TOLERANCE);
    }
}

#[test]
fn test_cos_lut_1024() {
    const TOLERANCE: f32 = 1e-5;
    const TABLE: &[f32] = table::SIN_COS_TABLE_1024;
    let angle: Vec<f32> = range(MIN_ANGLE_RANGE, MAX_ANGLE_RANGE)
        .map(to_rad)
        .collect();

    for angle in angle.iter() {
        assert_abs_diff_eq!(cos_lut(*angle, TABLE), angle.cos(), epsilon = TOLERANCE);
    }
}
