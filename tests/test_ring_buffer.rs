use dsp::RingBuffer;

type TestType = f32;

const BUFFER_SIZE: usize = 12;
const DEFAULT_VALUE: TestType = f32::MAX;

#[test]
fn new() {
    let buffer: RingBuffer<TestType, BUFFER_SIZE> = RingBuffer::new(DEFAULT_VALUE);
    assert_eq!(buffer.capacity(), BUFFER_SIZE);
}

#[test]
fn push_and_read() {
    let mut buffer: RingBuffer<TestType, BUFFER_SIZE> = RingBuffer::new(DEFAULT_VALUE);
    const INIT_VALUE_SIZE: usize = 8;
    const INIT_VALUE: [TestType; INIT_VALUE_SIZE] = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0];
    for i in INIT_VALUE {
        buffer.push(i);
    }

    let mut data: [TestType; BUFFER_SIZE] = [0.0; BUFFER_SIZE];
    for (i, value) in buffer.iter().enumerate() {
        data[i] = *value;
    }

    const EXPECTED: [TestType; BUFFER_SIZE] = [
        8.0,
        7.0,
        6.0,
        5.0,
        4.0,
        3.0,
        2.0,
        1.0,
        DEFAULT_VALUE,
        DEFAULT_VALUE,
        DEFAULT_VALUE,
        DEFAULT_VALUE,
    ];
    assert_eq!(data[..BUFFER_SIZE], EXPECTED);
}

#[test]
fn push_and_overwrite() {
    let mut buffer: RingBuffer<TestType, BUFFER_SIZE> = RingBuffer::new(DEFAULT_VALUE);
    const INIT_VALUE_SIZE: usize = BUFFER_SIZE;
    const INIT_VALUE: [TestType; INIT_VALUE_SIZE] = [
        1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0,
    ];
    for i in INIT_VALUE {
        buffer.push(i);
    }

    const NEW_VALUE: [TestType; 3] = [13.0, 14.0, 15.0];
    for i in NEW_VALUE {
        buffer.push(i);
    }

    let mut data: [TestType; BUFFER_SIZE] = [0.0; BUFFER_SIZE];
    for (i, value) in buffer.iter().enumerate() {
        data[i] = *value;
    }

    const EXPECTED: [TestType; INIT_VALUE_SIZE] = [
        15.0, 14.0, 13.0, 12.0, 11.0, 10.0, 9.0, 8.0, 7.0, 6.0, 5.0, 4.0,
    ];
    assert_eq!(data[..INIT_VALUE_SIZE], EXPECTED);
}

#[test]
fn push_read_push() {
    let mut buffer: RingBuffer<TestType, BUFFER_SIZE> = RingBuffer::new(DEFAULT_VALUE);
    const INIT_VALUE_SIZE: usize = BUFFER_SIZE;
    const INIT_VALUE: [TestType; INIT_VALUE_SIZE] = [
        1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0,
    ];
    for i in INIT_VALUE {
        buffer.push(i);
    }

    // Read the whole buffer
    let mut data: [TestType; BUFFER_SIZE] = [0.0; BUFFER_SIZE];
    for (i, value) in buffer.iter().enumerate() {
        data[i] = *value;
    }

    const NEW_VALUE: [TestType; 3] = [13.0, 14.0, 15.0];
    for i in NEW_VALUE {
        buffer.push(i);
    }

    let mut data: [TestType; BUFFER_SIZE] = [0.0; BUFFER_SIZE];
    for (i, value) in buffer.iter().enumerate() {
        data[i] = *value;
    }

    const EXPECTED: [TestType; INIT_VALUE_SIZE] = [
        15.0, 14.0, 13.0, 12.0, 11.0, 10.0, 9.0, 8.0, 7.0, 6.0, 5.0, 4.0,
    ];
    assert_eq!(data[..INIT_VALUE_SIZE], EXPECTED);
}

#[test]
fn read_multiple_time() {
    let mut buffer: RingBuffer<TestType, BUFFER_SIZE> = RingBuffer::new(DEFAULT_VALUE);
    const INIT_VALUE: [TestType; BUFFER_SIZE] = [
        1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0,
    ];
    for i in INIT_VALUE {
        buffer.push(i);
    }

    // Read the whole buffer twice
    let mut data_first: [TestType; BUFFER_SIZE] = [0.0; BUFFER_SIZE];
    for (i, value) in buffer.iter().enumerate() {
        data_first[i] = *value;
    }

    let mut data_second: [TestType; BUFFER_SIZE] = [0.0; BUFFER_SIZE];
    for (i, value) in buffer.iter().enumerate() {
        data_second[i] = *value;
    }

    let mut expected: [TestType; BUFFER_SIZE] = INIT_VALUE;
    expected.reverse();
    assert_eq!(data_first, data_second);
    assert_eq!(data_first, expected);
}

#[test]
fn clear() {
    let mut buffer: RingBuffer<TestType, BUFFER_SIZE> = RingBuffer::new(DEFAULT_VALUE);
    const INIT_VALUE: [TestType; BUFFER_SIZE] = [1.0; BUFFER_SIZE];
    for i in INIT_VALUE {
        buffer.push(i);
    }

    buffer.clear(DEFAULT_VALUE);

    let mut data: [TestType; BUFFER_SIZE] = [0.0; BUFFER_SIZE];
    for (i, value) in buffer.iter().enumerate() {
        data[i] = *value;
    }

    const EXPECTED: [TestType; BUFFER_SIZE] = [DEFAULT_VALUE; BUFFER_SIZE];
    assert_eq!(data, EXPECTED);
}
