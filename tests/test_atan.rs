use approx::assert_abs_diff_eq;

use dsp::math::atan;
use rand::Rng;

const TOLERANCE: f32 = 1e-6;
const INPUT_MIN: f32 = -1e6;
const INPUT_MAX: f32 = 1e6;

#[test]
fn test_atan() {
    let mut rng = rand::thread_rng();
    for _ in 0..10000 {
        let x: f32 = rng.gen_range(INPUT_MIN..INPUT_MAX);
        let y: f32 = rng.gen_range(INPUT_MIN..INPUT_MAX);
        assert_abs_diff_eq!(atan(x, y), y.atan2(x), epsilon = TOLERANCE);
    }
}

#[test]
fn test_atan_special_input() {
    assert!(atan(0.0, 0.0).is_nan());
    assert!(atan(f32::NAN, 1.0).is_nan());
    assert!(atan(1.0, f32::NAN).is_nan());
    assert_eq!(atan(f32::INFINITY, std::f32::consts::FRAC_PI_2), 0.0);
    assert_eq!(
        atan(f32::NEG_INFINITY, std::f32::consts::FRAC_PI_2),
        std::f32::consts::PI
    );
}
