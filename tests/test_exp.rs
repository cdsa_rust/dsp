use approx::assert_relative_eq;

use dsp::math::exp;
use rand::Rng;

const TOLERANCE: f32 = 2e-6;
const INPUT_MIN: f32 = -20.0;
const INPUT_MAX: f32 = 20.0;

#[test]
fn test_exp() {
    let mut rng = rand::thread_rng();
    for _ in 0..10000 {
        let x: f32 = rng.gen_range(INPUT_MIN..INPUT_MAX);
        assert_relative_eq!(exp(x), x.exp(), max_relative = TOLERANCE);
    }
}

#[test]
fn test_special_inputs() {
    assert_relative_eq!(exp(f32::NEG_INFINITY), 0.0, max_relative = TOLERANCE);
    assert!(exp(f32::INFINITY).is_infinite());
    assert!(exp(f32::NAN).is_nan());
}
