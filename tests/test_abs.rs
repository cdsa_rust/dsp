use dsp::math::abs;

#[test]
fn test_abs() {
    assert_eq!(abs(0.80315), 0.80315);
    assert_eq!(abs(-0.80315), 0.80315);
    assert_eq!(abs(2.6378377e38), 2.6378377e38);
    assert_eq!(abs(-2.6378377e38), 2.6378377e38);
    assert_eq!(abs(-0.80315), 0.80315);
    assert_eq!(abs(0.0), 0.0);
    assert_eq!(abs(-0.0), 0.0);
    assert_eq!(abs(f32::INFINITY), f32::INFINITY);
    assert_eq!(abs(f32::NEG_INFINITY), f32::INFINITY);
    assert!(abs(f32::NAN).is_nan());
}
