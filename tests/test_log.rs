use approx::assert_abs_diff_eq;

use dsp::math::{ln, log10, log2};
use rand::Rng;

const TOLERANCE: f32 = 1e-4;
const INPUT_MIN: f32 = 1e-9;
const INPUT_MAX: f32 = 1e9;

#[test]
fn test_ln() {
    let mut rng = rand::thread_rng();
    for _ in 0..100000 {
        let x: f32 = rng.gen_range(INPUT_MIN..INPUT_MAX);
        assert_abs_diff_eq!(ln(x), x.ln(), epsilon = TOLERANCE);
    }
}

#[test]
fn test_ln_special_input() {
    let mut rng = rand::thread_rng();
    for _ in 0..100000 {
        let x: f32 = rng.gen_range(INPUT_MIN..INPUT_MAX) * -1.0;
        assert!(ln(x).is_nan());
    }

    assert!(ln(f32::NAN).is_nan());
    assert!(ln(f32::INFINITY).is_infinite());
    assert!(ln(f32::NEG_INFINITY).is_nan());
    assert!(ln(0.0).is_infinite());
}

#[test]
fn test_log10() {
    let mut rng = rand::thread_rng();
    for _ in 0..100000 {
        let x: f32 = rng.gen_range(INPUT_MIN..INPUT_MAX);
        assert_abs_diff_eq!(log10(x), x.log(10.0), epsilon = TOLERANCE);
    }
}

#[test]
fn test_log10_special_input() {
    let mut rng = rand::thread_rng();
    for _ in 0..100000 {
        let x: f32 = rng.gen_range(INPUT_MIN..INPUT_MAX) * -1.0;
        assert!(log10(x).is_nan());
    }

    assert!(log10(f32::NAN).is_nan());
    assert!(log10(f32::INFINITY).is_infinite());
    assert!(log10(f32::NEG_INFINITY).is_nan());
    assert!(log10(0.0).is_infinite());
}

#[test]
fn test_log2() {
    let mut rng = rand::thread_rng();
    for _ in 0..100000 {
        let x: f32 = rng.gen_range(INPUT_MIN..INPUT_MAX);
        assert_abs_diff_eq!(log2(x), x.log(2.0), epsilon = TOLERANCE);
    }
}

#[test]
fn test_log2_special_input() {
    let mut rng = rand::thread_rng();
    for _ in 0..100000 {
        let x: f32 = rng.gen_range(INPUT_MIN..INPUT_MAX) * -1.0;
        assert!(log2(x).is_nan());
    }

    assert!(log2(f32::NAN).is_nan());
    assert!(log2(f32::INFINITY).is_infinite());
    assert!(log2(f32::NEG_INFINITY).is_nan());
    assert!(log2(0.0).is_infinite());
}
