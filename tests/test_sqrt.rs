use approx::assert_relative_eq;
use dsp::math::{sqrt, sqrt_newton_f32};
use rand::Rng;

const TOLERANCE_F32: f32 = 2.0 * f32::EPSILON;

#[test]
fn test_sqrt_rand_big_value() {
    let mut rng = rand::thread_rng();
    for _ in 0..10000 {
        let x = rng.gen_range(0.0..f32::MAX);
        let y = sqrt_newton_f32(x);
        let z = sqrt(x);

        assert_relative_eq!(x.sqrt(), y, max_relative = TOLERANCE_F32);
        assert_relative_eq!(x.sqrt(), z, max_relative = TOLERANCE_F32);
    }
}

#[test]
fn test_sqrt_rand_small_value() {
    let mut rng = rand::thread_rng();
    for _ in 0..10000 {
        let x = rng.gen_range(0.0..1.0);
        let y = sqrt_newton_f32(x);

        assert_relative_eq!(x.sqrt(), y, max_relative = TOLERANCE_F32);
    }
}

#[test]
fn test_sqrt_fuzz_negative_value() {
    let mut rng = rand::thread_rng();
    for _ in 0..10000 {
        let x = rng.gen_range(f32::MIN..0.0);
        let y = sqrt_newton_f32(x);

        assert!(y.is_nan());
    }
}

#[test]
fn test_sqrt_inf() {
    assert_eq!(sqrt_newton_f32(f32::INFINITY), f32::INFINITY);
    assert!(sqrt_newton_f32(f32::NEG_INFINITY).is_nan());
}

#[test]
fn test_sqrt_zero() {
    assert_eq!(sqrt_newton_f32(0.0), 0.0);
    assert_eq!(sqrt_newton_f32(-0.0), -0.0);
}
