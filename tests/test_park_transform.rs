#![cfg(test)]
use approx::assert_abs_diff_eq;
use dsp::math::table::SIN_COS_TABLE_1024;
use dsp::math::{park_transform, park_transform_lut};
use rand::Rng;

const TOLERANCE: f32 = 1e-6;
const TOLERANCE_LUT: f32 = 10e-6;

fn expected_park_transform(alpha: f32, beta: f32, theta: f32) -> (f32, f32) {
    let d = alpha * theta.cos() + beta * theta.sin();
    let q = beta * theta.cos() - alpha * theta.sin();
    (d, q)
}

fn test_park_transform_random_value() {
    let mut rng = rand::thread_rng();
    let theta = rng.gen_range(-2.0 * std::f32::consts::PI..2.0 * std::f32::consts::PI);
    let alpha = rand::random::<f32>();
    let beta = rand::random::<f32>();

    let (d, q) = park_transform(alpha, beta, theta);

    let (expected_d, expected_q) = expected_park_transform(alpha, beta, theta);
    assert_abs_diff_eq!(d, expected_d, epsilon = TOLERANCE);
    assert_abs_diff_eq!(q, expected_q, epsilon = TOLERANCE);
}

fn test_park_transform_lut_random_value() {
    let mut rng = rand::thread_rng();
    let theta = rng.gen_range(-2.0 * std::f32::consts::PI..2.0 * std::f32::consts::PI);
    let alpha = rand::random::<f32>();
    let beta = rand::random::<f32>();

    let (d, q) = park_transform_lut(alpha, beta, theta, SIN_COS_TABLE_1024);

    let (expected_d, expected_q) = expected_park_transform(alpha, beta, theta);
    assert_abs_diff_eq!(d, expected_d, epsilon = TOLERANCE_LUT);
    assert_abs_diff_eq!(q, expected_q, epsilon = TOLERANCE_LUT);
}

#[test]
fn test_park_transform() {
    for _ in 0..10000 {
        test_park_transform_random_value();
    }
}

#[test]
fn test_park_transform_lut() {
    for _ in 0..10000 {
        test_park_transform_lut_random_value();
    }
}
