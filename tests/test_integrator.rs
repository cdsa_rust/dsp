#![cfg(test)]

use approx::assert_abs_diff_eq;
use dsp::math::{Integrator, Integrator3rdOrder};
const TOLERANCE: f32 = 1e-6;

#[test]
fn test_integrator() {
    // Generate a vector of input data
    let input_data: Vec<f32> = vec![1.0; 100];

    // Generate a vector of expected output data
    let mut expected_output_data: Vec<f32> = Vec::new();
    let dt = 0.01;
    let mut sum = 0.0;
    for i in 0..input_data.len() {
        sum += dt * (input_data[i] + if i > 0 { input_data[i - 1] } else { 0.0 }) / 2.0;
        expected_output_data.push(sum);
    }

    // Test
    let mut output: Vec<f32> = Vec::new();
    let mut integrator = Integrator::new(dt);

    for i in input_data.iter() {
        let x = integrator.update(*i);
        output.push(x);
    }

    // Assert
    assert_eq!(output.len(), expected_output_data.len());
    for i in 0..output.len() {
        assert_eq!(output[i], expected_output_data[i]);
    }
}

#[test]
fn test_third_order_integrator() {
    // Generate a vector of input data
    let input_data: Vec<f32> = vec![1.0; 10];

    // Generate a vector of expected output data
    let expected_output: Vec<f32> = vec![
        0.0, 0.0001917, 0.00025, 0.00035, 0.00045, 0.00055, 0.00065, 0.00075, 0.00085, 0.00095,
    ];
    let dt = 100e-6;

    // Test
    let mut output: Vec<f32> = Vec::new();
    let mut integrator = Integrator3rdOrder::new(dt);

    for i in input_data.iter() {
        let x = integrator.get_output();
        integrator.update_input(*i);
        output.push(x);
    }

    // Assert
    assert_eq!(output.len(), expected_output.len());
    for i in 0..output.len() {
        assert_abs_diff_eq!(output[i], expected_output[i], epsilon = TOLERANCE);
    }
}
