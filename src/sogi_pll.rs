use crate::math::{abs, angle_to_0_2pi, park_transform, Integrator, Integrator3rdOrder};

pub struct SogiPll {
    sogi: Sogi,
    dq_pll: DqPll,
}

struct Sogi {
    k: f32, // SOGI Gain
    integrator_va: Integrator3rdOrder,
    integrator_vb: Integrator3rdOrder,
    va: f32,
    vb: f32,
}

struct DqPll {
    center_freq: f32, // Often labeled as omega_n
    kp: f32,
    ki: f32,
    integrator_pi: Integrator,
    integrator: Integrator,
    theta: f32, // PLL output
}

impl SogiPll {
    /// Create a new SogiPll instance.
    /// center_freq: Center frequency of the PLL in Rad/s
    /// k_sogi: SOGI gain
    /// kp: Proportional gain of the PLL
    /// ki: Integral gain of the PLL
    /// dt: Sample time of the PLL
    pub fn new(center_freq: f32, k_sogi: f32, kp: f32, ki: f32, dt: f32) -> SogiPll {
        SogiPll {
            sogi: Sogi {
                k: k_sogi,
                integrator_va: Integrator3rdOrder::new(dt),
                integrator_vb: Integrator3rdOrder::new(dt),
                va: 0.0,
                vb: 0.0,
            },

            dq_pll: DqPll {
                center_freq,
                kp,
                ki,
                integrator_pi: Integrator::new(dt),
                integrator: Integrator::new(dt),
                theta: 0.0,
            },
        }
    }

    // Output: (theta, |V'|)
    pub fn update(&mut self, input: f32) -> (f32, f32) {
        // SOGI va
        self.sogi.va = self.sogi.integrator_va.get_output();
        self.sogi.vb = self.sogi.integrator_vb.get_output();

        // DQ PLL
        // Using d as the PLL output. When using an offset of PI/2 on theta, the calculation are less precise.
        // This is why the calculation is not exactly following the MATLAB model.
        let (d, q) = park_transform(self.sogi.va, self.sogi.vb, self.dq_pll.theta);
        let q = abs(q);
        let omega = d * self.dq_pll.kp
            + self.dq_pll.integrator_pi.update(d * self.dq_pll.ki)
            + self.dq_pll.center_freq;
        let theta = self.dq_pll.integrator.update(omega);
        self.dq_pll.theta = angle_to_0_2pi(theta);

        // Update SOGI integrators
        let v = ((input - self.sogi.va) * self.sogi.k - self.sogi.vb) * omega;
        self.sogi.integrator_va.update_input(v);
        self.sogi.integrator_vb.update_input(self.sogi.va * omega);

        (self.dq_pll.theta, q)
    }
}
