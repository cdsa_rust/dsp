use num::Float;

pub struct PIDController<T>
where
    T: Float,
{
    kp: T,
    ki: T,
    kd: T,
    error_integral: T,
    current_error: T,
    prev_error: T,
    sample_time: T,
    half_sample_time: T,
    positive_saturation: T,
    negative_saturation: T,
}

/// PID controller configuration.
/// Set saturation to infinity to disable anti-windup.
pub struct PIDControllerConfig<T>
where
    T: Float,
{
    /// Proportional gain.
    pub kp: T,
    /// Integral gain.
    pub ki: T,
    /// Derivative gain.
    pub kd: T,
    /// Sample time in seconds.
    pub sample_time: T,
    /// Positive saturation limit.
    pub positive_saturation: T,
    /// Negative saturation limit.
    pub negative_saturation: T,
}

impl<T> PIDController<T>
where
    T: Float,
{
    pub fn new(config: &PIDControllerConfig<T>) -> PIDController<T> {
        PIDController {
            kp: config.kp,
            ki: config.ki,
            kd: config.kd,
            error_integral: T::zero(),
            current_error: T::zero(),
            prev_error: T::zero(),
            sample_time: config.sample_time,
            half_sample_time: config.sample_time / T::from(2.0).unwrap(),
            positive_saturation: config.positive_saturation,
            negative_saturation: config.negative_saturation,
        }
    }

    pub fn update(&mut self, error: T) -> T {
        // Proportional
        let proportional = self.kp * error;
        // Derivative
        let derivative = self.kd * (error - self.prev_error) / self.sample_time;

        // Integral
        if self.is_saturating(proportional) {
            // Clamping integral error
        } else {
            self.error_integral =
                self.error_integral + self.half_sample_time * (error + self.prev_error);
        }
        let integral = self.ki * self.error_integral;

        // Save error
        self.prev_error = error;

        // Output
        let output = proportional + integral + derivative;

        // Saturation
        if output > self.positive_saturation {
            return self.positive_saturation;
        } else if output < self.negative_saturation {
            return self.negative_saturation;
        }
        output
    }

    pub fn reset(&mut self) {
        self.error_integral = T::zero();
        self.current_error = T::zero();
        self.prev_error = T::zero();
    }

    fn is_saturating(&self, proportional: T) -> bool {
        (proportional > self.positive_saturation) || (proportional < self.negative_saturation)
    }
}
