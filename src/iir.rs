use crate::RingBuffer;
use num::Float;

/// IIR Filter
pub struct IIRFilter<'taps, T, const N: usize>
where
    T: Float,
{
    a: &'taps [T; N],
    b: &'taps [T; N],
    input_buffer: RingBuffer<T, N>,
    feedback_buffer: RingBuffer<T, N>,
}

impl<'taps, T, const N: usize> IIRFilter<'taps, T, N>
where
    T: Float,
{
    pub fn new(numerator: &'taps [T; N], denominator: &'taps [T; N]) -> Self {
        IIRFilter {
            a: denominator,
            b: numerator,
            input_buffer: RingBuffer::new(T::zero()),
            feedback_buffer: RingBuffer::new(T::zero()),
        }
    }

    pub fn filter(&mut self, data: &T) -> T {
        self.input_buffer.push(*data);

        // Input
        let mut b_sum = T::zero();
        for (tap, value) in self.b.iter().zip(self.input_buffer.iter()) {
            b_sum = b_sum + (*tap) * (*value);
        }
        // Feedback
        let mut a_sum = T::zero();
        for (tap, value) in self.a[1..].iter().zip(self.feedback_buffer.iter()) {
            a_sum = a_sum + (*tap) * (*value);
        }

        // Output
        let output = (b_sum - a_sum) / self.a[0];
        self.feedback_buffer.push(output);
        output
    }
}
