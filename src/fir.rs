use crate::RingBuffer;
use core::cmp::min;
use num::Float;

/// FIR Filter object
pub struct FIRFilter<'taps, T, const N: usize>
where
    T: Float,
{
    taps: &'taps [T; N],
    buffer: RingBuffer<T, N>,
}

impl<'taps, T, const N: usize> FIRFilter<'taps, T, N>
where
    T: Float,
{
    pub fn new(taps: &'taps [T; N]) -> Self {
        FIRFilter {
            taps,
            buffer: RingBuffer::new(T::zero()),
        }
    }

    pub fn filter(&mut self, data: &T) -> T {
        self.buffer.push(*data);
        let mut output = T::zero();
        for (tap, value) in self.taps.iter().zip(self.buffer.iter()) {
            output = output + (*tap) * (*value);
        }
        output
    }
}

/// Polyphase FIR Filter for decimation
pub struct FIRFilterDecimate<'taps, T, const N: usize, const RATIO: usize>
where
    T: Float,
{
    taps: &'taps [T; N],
    buffer: [RingBuffer<T, N>; RATIO],
    sum: T,
    cursor: usize,
}

impl<'taps, T, const N: usize, const RATIO: usize> FIRFilterDecimate<'taps, T, N, RATIO>
where
    T: Float,
{
    //noinspection RsAssertEqual Reason: assert_eq! is not const fn.
    const _COMPTIME_SIZE_CHECK: () = assert!(
        N % RATIO == 0,
        "Number of taps must be a multiple of the decimation / interpolation ratio."
    );

    pub fn new(taps: &'taps [T; N]) -> Self {
        FIRFilterDecimate {
            taps,
            buffer: [RingBuffer::new(T::zero()); RATIO],
            sum: T::zero(),
            cursor: 0,
        }
    }

    pub fn decimate(&mut self, data: &T) -> Option<T> {
        let cursor = self.cursor;
        let buffer = &mut self.buffer[cursor];
        buffer.push(*data);

        let mut output: Option<T> = None;

        // Calculate FIR
        let mut fir_output = T::zero();
        for (tap_index, value) in ((cursor..N).step_by(RATIO)).zip(buffer.iter()) {
            fir_output = fir_output + self.taps[tap_index] * (*value);
        }

        self.sum = self.sum + fir_output;
        self.cursor = (self.cursor + 1) % RATIO;

        if cursor == 0 {
            output.replace(self.sum);
            self.sum = T::zero();
        }
        output
    }
}

/// Polyphase FIR Filter for interpolation
pub struct FIRFilterInterpolate<'taps, T, const N: usize, const RATIO: usize>
where
    T: Float,
{
    taps: &'taps [T; N],
    buffer: [RingBuffer<T, N>; RATIO],
    scaling: T,
}

impl<'taps, T, const N: usize, const RATIO: usize> FIRFilterInterpolate<'taps, T, N, RATIO>
where
    T: Float,
{
    //noinspection RsAssertEqual Reason: assert_eq! is not const fn.
    const _COMPTIME_SIZE_CHECK: () = assert!(
        N % RATIO == 0,
        "Number of taps must be a multiple of the decimation / interpolation ratio."
    );

    pub fn new(taps: &'taps [T; N]) -> Self {
        FIRFilterInterpolate {
            taps,
            buffer: [RingBuffer::new(T::zero()); RATIO],
            scaling: T::from(RATIO).unwrap(),
        }
    }

    /// Interpolate the given data and write the result to the given output buffer.
    /// Returns the number of output samples written.
    pub fn interpolate(&mut self, data: &T, output: &mut [T]) -> usize {
        let out_end = min(output.len(), RATIO);

        // Calculate new points
        for (i, point) in output[..out_end].iter_mut().enumerate() {
            let buffer = &mut self.buffer[i];
            buffer.push(*data);

            // Calculate FIR
            let mut output = T::zero();
            for (tap_index, value) in ((i..N).step_by(RATIO)).zip(buffer.iter()) {
                output = output + self.taps[tap_index] * (*value);
            }

            // Save new value
            *point = output * self.scaling;
        }

        out_end
    }
}
