/// A simple integrator that uses the trapezoidal rule.
pub struct Integrator {
    x: f32,
    last_x: f32,
    dt_over_2: f32,
}

impl Integrator {
    pub fn new(dt: f32) -> Integrator {
        Integrator {
            x: 0.0,
            last_x: 0.0,
            dt_over_2: dt * 0.5,
        }
    }

    pub fn update(&mut self, x: f32) -> f32 {
        self.x += self.dt_over_2 * (self.last_x + x);
        self.last_x = x;
        self.x
    }

    pub fn reset(&mut self) {
        self.x = 0.0;
        self.last_x = 0.0;
    }
}

/// Third Order Integrator
/// This method is useful to break algebraic loops in a system.
pub struct Integrator3rdOrder {
    x: [f32; 3],
    dt_over_12: f32,
}

impl Integrator3rdOrder {
    pub fn new(dt: f32) -> Integrator3rdOrder {
        Integrator3rdOrder {
            x: [0.0; 3],
            dt_over_12: dt / 12.0,
        }
    }

    pub fn update(&mut self, x: f32) -> f32 {
        let output = self.get_output();
        self.update_input(x);
        output
    }

    pub fn get_output(&mut self) -> f32 {
        23.0 * self.x[0] - 16.0 * self.x[1] + 5.0 * self.x[2]
    }

    pub fn update_input(&mut self, x: f32) {
        self.x[2] = self.x[1];
        self.x[1] = self.x[0];
        self.x[0] += x * self.dt_over_12;
    }

    pub fn reset(&mut self) {
        self.x = [0.0; 3];
    }
}
