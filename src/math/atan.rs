/// This module implement the atan function.

/// Returns the four quadrant arc tangent of x and y. The result is between 0 and 2*pi.
/// Taken from CMSIS-DSP.
pub fn atan(x: f32, y: f32) -> f32 {
    if x > 0.0 {
        return _atan(y / x);
    }

    if x < 0.0 {
        return if y > 0.0 {
            _atan(y / x) + core::f32::consts::PI
        } else if y < 0.0 {
            _atan(y / x) - core::f32::consts::PI
        } else if y.is_sign_negative() {
            -core::f32::consts::PI
        } else {
            core::f32::consts::PI
        };
    }
    if x == 0.0 {
        if y > 0.0 {
            return core::f32::consts::FRAC_PI_2;
        }
        if y < 0.0 {
            return -core::f32::consts::FRAC_PI_2;
        }
    }

    f32::NAN
}

const ATAN_COEFFICIENTS: [f32; 10] = [
    0.0,
    1.000_000_1,
    -0.000_022_894_137,
    -0.332_808_64,
    -0.004_404_814_4,
    0.216_221_75,
    -0.020_750_483,
    -0.174_526_33,
    0.134_055_72,
    -0.032_366_414,
];

/// atan for argument between in 0 and 1.0
fn _atan_limited(x: f32) -> f32 {
    const N: usize = ATAN_COEFFICIENTS.len() - 1;

    let mut res = ATAN_COEFFICIENTS[N];
    for i in 1..ATAN_COEFFICIENTS.len() {
        res = x * res + ATAN_COEFFICIENTS[N - i];
    }
    res
}

fn _atan(x: f32) -> f32 {
    let mut sign = false;
    let mut res = 0.0;
    let mut x = x;

    if x < 0.0 {
        sign = true;
        x = -x;
    }

    if x > 1.0 {
        x = 1.0 / x;
        res = core::f32::consts::FRAC_PI_2 - _atan_limited(x);
    } else {
        res += _atan_limited(x);
    }

    if sign {
        res = -res;
    }

    res
}
