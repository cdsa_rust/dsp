use crate::math::{cos_lut, sin_cos, sin_lut};

/// Park transform
/// Outputs: (d, q)
pub fn park_transform(alpha: f32, beta: f32, theta: f32) -> (f32, f32) {
    let (sin_theta, cos_theta) = sin_cos(theta);
    let d = alpha * cos_theta + beta * sin_theta;
    let q = beta * cos_theta - alpha * sin_theta;

    (d, q)
}

/// Park transform using a lookup table for sin and cos
/// Outputs: (d, q)
pub fn park_transform_lut(alpha: f32, beta: f32, theta: f32, table: &'static [f32]) -> (f32, f32) {
    let sin_theta = sin_lut(theta, table);
    let cos_theta = cos_lut(theta, table);
    let d = alpha * cos_theta + beta * sin_theta;
    let q = beta * cos_theta - alpha * sin_theta;

    (d, q)
}
