//! Square root function for single precision floating point numbers.
//! Implementation vary depending on the target architecture.
//! Use VSQRT.F32 instruction on ARMv7-M and ARMv8-M with FPU.

/// Returns the square root of a number.
pub fn sqrt(value: f32) -> f32 {
    #[cfg(all(target_feature = "sse", target_arch = "x86_64"))]
    {
        use core::arch::x86_64::*;
        unsafe {
            let x = _mm_set_ss(value);
            let x = _mm_sqrt_ss(x);
            _mm_cvtss_f32(x)
        }
    }

    #[cfg(not(any(target_feature = "sse", vsqrt)))]
    {
        sqrt_newton_f32(value)
    }

    #[cfg(vsqrt)]
    {
        unsafe { _vsqrt_f32(value) }
    }
}

/// Square root function using Newton method.
/// Assume the float is IEEE 754 single precision.
/// Reference: MATH TOOLKIT FOR REAL-TIME PROGRAMMING by Jack Crenshaw
pub fn sqrt_newton_f32(value: f32) -> f32 {
    const PRECISION: u32 = 3; // Number of iterations to perform
    const EXPONENT_SHIFT: u32 = 23;
    const EXPONENT_BIAS: u32 = 127 << EXPONENT_SHIFT;
    const MANTISSA_MASK: u32 = 0x007FFFFF;
    const SIGN_MASK: u32 = 0x80000000;
    const INFINITY: u32 = 0x7f800000;
    const FACTOR: f32 = core::f32::consts::FRAC_1_SQRT_2;
    const A: f32 = 0.417_319_24;
    const B: f32 = 0.590_178_55;

    // Disassemble bits
    let x = value.to_bits();

    // Handle special cases
    if x == INFINITY {
        return f32::INFINITY;
    }

    if x == 0 || x == SIGN_MASK {
        return value;
    }

    if ((x & SIGN_MASK) > 0) || value.is_nan() {
        return f32::NAN;
    }

    // Extract exponent and mantissa
    let mut exponent: i32 = ((x.wrapping_sub(EXPONENT_BIAS)) >> EXPONENT_SHIFT) as i32;
    let mantissa: f32 = f32::from_bits((x & MANTISSA_MASK) + EXPONENT_BIAS);

    // Generate initial guess
    let mut root = A + B * mantissa;

    // Calculate the root
    for _ in 0..PRECISION {
        root = 0.5 * (mantissa / root + root);
    }

    // Force exponent to be even
    if (exponent & 1) > 0 {
        root *= FACTOR;
    }

    // Halve the exponent
    exponent >>= 1;

    // Reassemble bits
    let exponent: u32 = ((exponent as u32) << EXPONENT_SHIFT) + EXPONENT_BIAS;
    let result = ((root.to_bits() & MANTISSA_MASK) + exponent) & !SIGN_MASK;

    f32::from_bits(result)
}

#[cfg(vsqrt)]
use core::arch::global_asm;

#[cfg(vsqrt)]
global_asm!(include_str!("vsqrt.s"));

#[cfg(vsqrt)]
extern "C" {
    fn _vsqrt_f32(x: f32) -> f32;
}
