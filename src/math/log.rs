//! Logarithm function for single precision floating point numbers.
//! Reference: MATH TOOLKIT FOR REAL-TIME PROGRAMMING by Jack Crenshaw

/// Returns the base 2 logarithm of a number.
pub fn log2(value: f32) -> f32 {
    ln(value) * core::f32::consts::LOG2_E
}

/// Returns the base 10 logarithm of a number.
pub fn log10(value: f32) -> f32 {
    ln(value) * core::f32::consts::LOG10_E
}

/// Returns the natural logarithm of a number.
pub fn ln(value: f32) -> f32 {
    if value < 1.0 {
        return -1.0 * _ln(1.0 / value);
    }
    _ln(value)
}

fn _ln(value: f32) -> f32 {
    const LN2: f32 = core::f32::consts::LN_2;
    const EXPONENT_SHIFT: u32 = 23;
    const EXPONENT_BIAS: u32 = 127 << EXPONENT_SHIFT;
    const MANTISSA_MASK: u32 = 0x007FFFFF;
    const SIGN_MASK: u32 = 0x80000000;

    // Disassemble bits
    let x = value.to_bits();

    // Check for special values
    if ((x & SIGN_MASK) > 0) || value.is_nan() {
        return f32::NAN;
    }
    if value == 0.0 {
        return f32::NEG_INFINITY;
    }
    if value == f32::INFINITY {
        return f32::INFINITY;
    }

    // Extract exponent and mantissa
    let exponent: f32 = ((x.wrapping_sub(EXPONENT_BIAS)) >> EXPONENT_SHIFT) as f32;
    let mantissa: f32 = f32::from_bits((x & MANTISSA_MASK) + EXPONENT_BIAS);

    // compute the log of the mantissa only
    let retval = __ln(mantissa);

    // rebuild the result
    LN2 * (exponent + retval / LN2)
}

fn __ln(x: f32) -> f32 {
    const LIMIT1: f32 = 0.879_559_6; // 0.5^(1/5)
    const LIMIT2: f32 = 0.659_754; // 0.5^(3/5)
    const K1: f32 = 0.757_858_3; // 0.5^(2/5)
    const K2: f32 = 0.574_349_16; // 0.5^(4/5)
    const LN_K: f32 = -0.138_629_44; // ln(0.5^(1/5)

    if x >= LIMIT1 {
        ___ln(x)
    } else if x >= LIMIT2 {
        ___ln(x / K1) + 2.0 * LN_K
    } else {
        ___ln(x / K2) + 4.0 * LN_K
    }
}

fn ___ln(x: f32) -> f32 {
    const A: f32 = 1.0;
    const B: f32 = -0.2672435;
    const C: f32 = -0.600576;
    let z = (x - 1.0) / (x + 1.0);
    let z2 = z * z;
    2.0 * z * (A + B * z2) / (1.0 + C * z2)
}
