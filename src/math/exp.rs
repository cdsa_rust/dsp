/// Returns the exponential of a value.
pub fn exp(value: f32) -> f32 {
    if value < 0.0 {
        1.0 / _exp(-1.0 * value)
    } else {
        _exp(value)
    }
}

fn _exp(value: f32) -> f32 {
    const ROOT_E: f32 = 1.284_025_4;
    const A: f32 = 1.000000034750736;
    const B: f32 = 0.499_998;
    const C: f32 = 0.166_704_07;
    const D: f32 = 0.041_365_42;
    const E: f32 = 9.419_274E-3;

    let mut x = value * 4.0;
    let n = x as u32; // Value is always positive.
    x -= n as f32;
    x /= 4.0;
    let retval = ((((x * E + D) * x + C) * x + B) * x + A) * x + 1.0;
    let int_part = power(ROOT_E, n);

    int_part * retval
}

/// Return the power of a value. Only for positive integers.
pub fn power(value: f32, n: u32) -> f32 {
    let mut retval = 1.0;
    let mut factor = value;
    let mut n = n;

    while n != 0 {
        if n & 1 != 0 {
            retval *= factor;
        }
        factor *= factor;
        n >>= 1;
    }
    retval
}
