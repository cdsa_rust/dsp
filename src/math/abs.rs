//! Absolute function for single precision floating point numbers.
//! Implementation vary depending on the target architecture.
//! Use VABS.F32 instruction on ARMv7-M and ARMv8-M with FPU.

pub fn abs(value: f32) -> f32 {
    #[cfg(not(vabs))]
    {
        const ABS_MASK: u32 = 0x7FFFFFFF;
        f32::from_bits(f32::to_bits(value) & ABS_MASK)
    }

    #[cfg(vabs)]
    {
        unsafe { _vabs_f32(value) }
    }
}

#[cfg(vabs)]
use core::arch::global_asm;

#[cfg(vabs)]
global_asm!(include_str!("vabs.s"));

#[cfg(vabs)]
extern "C" {
    fn _vabs_f32(x: f32) -> f32;
}
