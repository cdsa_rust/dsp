/// This module implements the sine and cosine functions.
/// Multiple techniques are used to implement the functions:
/// - sine and cosine are implemented using Taylor series.
/// - sine and cosine are implemented using lookup table.

const HALF_PI: f32 = core::f32::consts::PI / 2.0;
const TWO_OVER_PI: f32 = 2.0 / core::f32::consts::PI;
const TWO_PI: f32 = core::f32::consts::PI * 2.0;
const ONE_OVER_TWO_PI: f32 = 1.0 / (core::f32::consts::PI * 2.0);

/// Returns the sine of an angle in radian.
/// Best precision when angle is between -2pi and 2pi.
/// Any value with a magnitude greater than i32::MAX will return -inf.
pub fn sin(angle: f32) -> f32 {
    // handle invalid input
    if angle.is_nan() || angle.is_infinite() {
        return f32::NAN;
    }

    // The precision is not good for negative angles. Force to operate between 0 and 2pi.
    let angle = angle_to_0_2pi(angle);

    // Find the quadrant we are in.
    // Q1 is [7pi/4, pi/4], Q2 is [pi/4, 3pi/4], Q2 is [3pi/4, 5pi/4], Q4 is [5pi/4, 7pi/4].
    let n = (angle * TWO_OVER_PI + 0.5) as u32;
    let x = angle - ((n as f32) * HALF_PI);
    let quadrant = n & 3;

    match quadrant {
        0 => _sine(x),
        1 => _cosine(x),
        2 => -_sine(x),
        3 => -_cosine(x),
        _ => unreachable!(),
    }
}

/// Returns the cosine of an angle in radian.
/// Any value with a magnitude greater than i32::MAX will return -inf.
pub fn cos(angle: f32) -> f32 {
    sin(angle + HALF_PI)
}

/// Returns the sine and cosine of an angle in radian.
/// Using 16 segments lookup table.
/// Any value with a magnitude greater than i32::MAX will return NaN.
pub fn sin_cos(angle: f32) -> (f32, f32) {
    const N_SEGMENT_MASK: u32 = 0xF;
    const PI_OVER_EIGHT: f32 = core::f32::consts::PI / 8.0;
    const EIGHT_OVER_PI: f32 = 8.0 / core::f32::consts::PI;
    const COS_22_5: f32 = 0.923_879_5;
    const SIN_22_5: f32 = 0.382_683_43;
    const ISQRT_2: f32 = core::f32::consts::FRAC_1_SQRT_2;
    const S1: f32 = 1.0 / 20.0;
    const S2: f32 = 1.0 / 6.0;
    const C1: f32 = 1.0 / 30.0;
    const C2: f32 = 1.0 / 12.0;
    const C3: f32 = 1.0 / 2.0;

    // handle invalid input
    if angle.is_nan() || angle.is_infinite() {
        return (f32::NAN, f32::NAN);
    }

    // The precision is not good for negative angles. Force to operate between 0 and 2pi.
    let angle = angle_to_0_2pi(angle);

    // Find the segment we are in.
    let n = (angle * EIGHT_OVER_PI + 0.5) as u32;
    let x = angle - ((n as f32) * PI_OVER_EIGHT);
    let segment: u32 = n & N_SEGMENT_MASK;

    let z = x * x;
    let s1 = ((z * S1 - 1.0) * z * S2 + 1.0) * x;
    let c1 = ((z * C1 + 1.0) * z * C2 - 1.0) * z * C3 + 1.0;

    match segment {
        0 => (s1, c1),
        1 => (
            COS_22_5 * s1 + SIN_22_5 * c1,
            -SIN_22_5 * s1 + COS_22_5 * c1,
        ),
        2 => (ISQRT_2 * s1 + ISQRT_2 * c1, -ISQRT_2 * (s1 - c1)),
        3 => (
            SIN_22_5 * s1 + COS_22_5 * c1,
            -COS_22_5 * s1 + SIN_22_5 * c1,
        ),
        4 => (c1, -s1),
        5 => (
            -SIN_22_5 * s1 + COS_22_5 * c1,
            -COS_22_5 * s1 - SIN_22_5 * c1,
        ),
        6 => (-ISQRT_2 * (s1 - c1), -ISQRT_2 * (s1 + c1)),
        7 => (
            -COS_22_5 * s1 + SIN_22_5 * c1,
            -SIN_22_5 * s1 - COS_22_5 * c1,
        ),
        8 => (-s1, -c1),
        9 => (
            -COS_22_5 * s1 - SIN_22_5 * c1,
            SIN_22_5 * s1 - COS_22_5 * c1,
        ),
        10 => (-ISQRT_2 * (s1 + c1), ISQRT_2 * (s1 - c1)),
        11 => (
            -SIN_22_5 * s1 - COS_22_5 * c1,
            COS_22_5 * s1 - SIN_22_5 * c1,
        ),
        12 => (-c1, s1),
        13 => (SIN_22_5 * s1 - COS_22_5 * c1, COS_22_5 * s1 + SIN_22_5 * c1),
        14 => (ISQRT_2 * (s1 - c1), ISQRT_2 * s1 + ISQRT_2 * c1),
        15 => (COS_22_5 * s1 - SIN_22_5 * c1, SIN_22_5 * s1 + COS_22_5 * c1),
        _ => unreachable!(),
    }
}

/// Returns the angle in radian between 0 and 2pi.
pub fn angle_to_0_2pi(angle: f32) -> f32 {
    if angle > i32::MAX as f32 {
        return f32::NAN;
    }
    let n_turn = (angle * ONE_OVER_TWO_PI) as i32;
    let mut out = angle - (n_turn as f32 * TWO_PI);
    if out < 0.0 {
        out += TWO_PI;
    }
    out
}

/// Returns the sine of an angle < pi/4 radian (45 degree).
fn _sine(angle: f32) -> f32 {
    const S1: f32 = 1.0 / (2.0 * 3.0);
    const S2: f32 = 1.0 / (4.0 * 5.0);
    const S3: f32 = 1.0 / (6.0 * 7.0);
    const S4: f32 = 1.0 / (8.0 * 9.0);
    let z = angle * angle;
    ((((S4 * z - 1.0) * S3 * z + 1.0) * S2 * z - 1.0) * S1 * z + 1.0) * angle
}

/// Returns the cosine of an angle < pi/4 radian (45 degree).
fn _cosine(angle: f32) -> f32 {
    const C1: f32 = 1.0 / (1.0 * 2.0);
    const C2: f32 = 1.0 / (3.0 * 4.0);
    const C3: f32 = 1.0 / (5.0 * 6.0);
    const C4: f32 = 1.0 / (7.0 * 9.0);
    let z = angle * angle;
    (((C4 * z - 1.0) * C3 * z + 1.0) * C2 * z - 1.0) * C1 * z + 1.0
}

/// Returns the sine of an angle in radian using a lookup table.
/// Based on CMSIS-DSP implementation
pub fn sin_lut(angle: f32, table: &'static [f32]) -> f32 {
    // The precision is not good for negative angles. Force to operate between 0 and 2pi.
    let angle = angle_to_0_2pi(angle);

    // Bring to [0.0 .. 1.0] range
    let normalized_angle = angle * ONE_OVER_TWO_PI;

    // Calculation of index of the table
    let fidx = normalized_angle * table.len() as f32;
    let idx = fidx as usize;
    let mut next_idx = idx + 1;

    if next_idx >= table.len() {
        next_idx -= table.len();
    }

    // fractional value calculation
    let fract = fidx - (idx as f32);

    // Read two nearest values of input value from the sin table
    let a = table[idx];
    let b = table[next_idx];

    // Linear interpolation process
    (1.0 - fract) * a + fract * b
}

/// Returns the cosine of an angle in radian using a lookup table.
pub fn cos_lut(angle: f32, table: &'static [f32]) -> f32 {
    sin_lut(angle + HALF_PI, table)
}
