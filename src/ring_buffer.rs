//! Ring buffer.
//! Buffer used for FIR, IIR, etc.

use core::iter::Iterator;

/// RingBuffer
#[derive(Copy, Clone)]
pub struct RingBuffer<T, const N: usize> {
    cursor: usize,
    storage: [T; N],
}

/// Return the next index value of the ring buffer when decrementing the given value.
/// Parameter max is the array size.
pub(crate) fn index_decrement(current: usize, max: usize) -> usize {
    (current + max - 1) % max
}

/// Return the next index value of the ring buffer when incrementing the given value.
/// Parameter max is the array size.
pub(crate) fn index_increment(current: usize, max: usize) -> usize {
    (current + 1) % max
}

/// Iterator
pub struct Iter<'a, T>
where
    T: 'a,
{
    pub(crate) index: usize,
    pub(crate) count: usize,
    pub(crate) storage: &'a [T],
}

impl<'a, T> Iterator for Iter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        if self.count > 0 {
            self.count -= 1;
            let item = &self.storage[self.index];
            self.index = index_decrement(self.index, self.storage.len());
            Some(item)
        } else {
            None
        }
    }
}

impl<T: Copy, const N: usize> RingBuffer<T, N> {
    /// Constructs a new, empty RingBuffer<T, N>. The underlying array is initialized with the provided
    /// default value.
    pub const fn new(default: T) -> Self {
        Self {
            cursor: 0,
            storage: [default; N],
        }
    }

    /// Returns the total number of elements the container can hold.
    pub fn capacity(&self) -> usize {
        N
    }

    /// Appends an element to the back of the container. Overwrite the oldest value if the container is full.
    pub fn push(&mut self, value: T) {
        self.cursor = index_increment(self.cursor, N);
        self.storage[self.cursor] = value;
    }

    /// Clears the container, removing all values.
    pub fn clear(&mut self, default: T) {
        self.cursor = 0;
        for i in 0..N {
            self.storage[i] = default;
        }
    }

    /// Get an iterator to access to whole container.
    /// The iterator always go through the whole container from the newest to the oldest value.
    pub fn iter(&self) -> Iter<'_, T> {
        Iter {
            index: self.cursor,
            count: N,
            storage: &self.storage,
        }
    }
}
