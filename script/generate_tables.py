import os
import numpy as np

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
TABLE_RS_PATH = f"{CURRENT_DIR}/../src/math/table.rs"


class FastSinCosTable:
    # Generate a table of sin(x) for x in [0, 2*pi]

    def __init__(self, size: int):
        self.size = size
        self.delta_x = (np.pi * 2.0) / self.size
        self.x1 = self.generate_x1()
        self.y1 = self.generate_y1()

    def generate_x1(self) -> np.ndarray:
        x1 = np.zeros(self.size, dtype=np.float64)
        for i in range(0, self.size):
            x1[i] = i * self.delta_x
        return x1

    def generate_y1(self) -> np.ndarray:
        y1 = np.zeros(self.size, dtype=np.float64)
        for i in range(0, self.size):
            y1[i] = np.sin(self.x1[i])
        return y1

    def print_table(self):
        code = f"pub const SIN_COS_TABLE_{self.size}: &[f32] = &{list(self.y1)};\n"
        return code


def main():
    with open(TABLE_RS_PATH, 'w', encoding='utf-8') as file:
        file.write("#![allow(clippy::excessive_precision)]\n"
                   "#![allow(clippy::approx_constant)]\n\n")

        for size in [32, 64, 128, 256, 512, 1024]:
            table = FastSinCosTable(size)
            file.write(table.print_table())
            file.write("\n")


if __name__ == '__main__':
    main()
